const axios = require("axios");
const BASE_URL = 'https://swapi.dev/api';

async function getPlanetResidents(results) {
    return await Promise.all(results.map(async (planet) => {
        const { residents } = planet;
        planet.residents = await getResidents(residents);
        return planet;
    }));
}

async function getResidents(residents) {
    return await Promise.all(residents.map(async (val) => {
        const resident = await axios.get(val);
        return resident.data.name;
    }));
}

async function getPeople() {
    const allPeople = [];
    let hasNext = true;
    let url = `${BASE_URL}/people`;

    while (hasNext) {
        const res = await axios.get(url);
        const { next, results } = res.data;
        allPeople.push(...results);
        if (next) {
            url = next;
        } else {
            hasNext = false;
        }
    }
    return allPeople;
}

async function getPlanets() {
    const allPlanets = [];
    let hasNext = true;
    let url = `${BASE_URL}/planets`;

    while (hasNext) {
        const res = await axios.get(url);
        const { next, results } = res.data;
        // fetch all residents for each planet in results
        const planetsWithResidents = await getPlanetResidents(results)
        allPlanets.push(...planetsWithResidents);
        if (next) {
            url = next;
        } else {
            hasNext = false;
        }
    }
    return allPlanets;
}

module.exports = {
    getPeople,
    getPlanets,
};
