const express = require('express');
const cors = require('cors');
const PORT = 3001;

const { getPeople, getPlanets } = require('./api');
const { handleSort } = require('./utils/sort');

const app = express();
app.use(cors());

app.get('/people', async (req, res) => {
  try {
    let sortBy = req.query.sortBy || '';
    let allPeople = await getPeople();
    const sortedPeople = handleSort(sortBy, allPeople);

    res.status(200).send(sortedPeople);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.get('/planets', async (req, res) => {
  try {
    let allPlanets = await getPlanets();

    res.status(200).send(allPlanets);
  } catch (err) {
    res.status(500).send(err);
  }
});

app.listen(PORT, () => {
  console.log(`listening on port ${PORT}`);
});
