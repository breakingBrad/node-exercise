function handleSort(sortByParam, allPeople) {
  // allow for case insensitivity in sortBy param
  let sortBy = sortByParam.toLowerCase();

  if (!sortBy) {
    return allPeople;
  }

  if (sortBy === 'name') {
    return sortByName(allPeople);
  }
  // sort by 'mass' or 'height'
  return sortByMassOrHeight(allPeople, sortBy);
}

function sortByMassOrHeight(allPeople, sortBy) {
  allPeople.forEach(person => {
    // split and rejoin to account for commas that may be present in values
    person[sortBy] = person[sortBy].split(',').join('');
    if (person[sortBy] === 'unknown') {
      // temporarily overwrite all 'unknown' values in order to sort them to bottom of the list
      person[sortBy] = 0;
    }
  });
  // sorting values in descending order
  allPeople.sort((a, b) => {
    let aSort = +a[sortBy];
    let bSort = +b[sortBy];
    return bSort - aSort;
  });
  // restoring 'unknown' values
  allPeople.forEach(val => {
    if (val[sortBy] === 0) {
      val[sortBy] = 'unknown';
    }
  });
  return allPeople;
}

function sortByName(allPeople) {
  allPeople.sort((a, b) => {
    let aName = a.name.toLowerCase();
    let bName = b.name.toLowerCase();
    if (aName < bName) {
      return -1;
    } else if (aName > bName) {
      return 1;
    }
  });
  return allPeople;
}

module.exports = { handleSort };